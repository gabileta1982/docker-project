FROM ubuntu:latest
LABEL maintainer="gabriela.ridella@gmail.com"
LABEL version="1"
LABEL description="Practica Dockerfile de contenido estatico"

# Configuracion de descargas y updates
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y wget apache2 php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip && apt autoremove && apt clean

# Configuracion de la web en el apache
ENV APPSERVERNAME principito.com
ENV APPALIAS www.principito.com

# Creamos el directorio app en el raiz
RUN mkdir -p /app

# Copiamos el fichero de vhost a sites-available
COPY default.conf /etc/apache2/sites-available/

# Creamos los volumenes
VOLUME ["/var/www/html/images"]

COPY entrypoint.sh /app/
RUN chmod +x /app/entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]

EXPOSE 80