# gabileta1982-docker

## Instrucciones

### Crear la imagen
```
sudo docker build -t principito:v1 .
```

### Correr el proxy-reverso
```
sudo docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

### Correr la imagen que creamos previamente
```
docker run -d --name principito-image -v $(pwd)/images:/var/www/html/images -e VIRTUAL_HOST=principito.com principito:v1
```

### Editar archivo hosts para mapear la URL al local host

### Copiar las imagenes al volumen de imagenes del apache con el nombre indicado
```
cp el_principito_header.jpeg el_principito_footer.jpeg images/
```