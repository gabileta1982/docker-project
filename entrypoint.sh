#!/bin/bash
set -e

# Vaciamos directorio /var/www/html
if [ -f /var/www/html/index.html ]; then
rm -fr /var/www/html/index.html
fi

# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'
sed -i 's/SERVERNAME/'"$APPSERVERNAME"'/' /etc/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /etc/apache2/sites-available/default.conf

cat <<EOF > /var/www/html/index.html
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>Le Petit Prince</title>
</head>

<body style="background-color:MidnightBlue;color:DarkGoldenRod;text-align:center;">
<h1>El Principito</h1>
<section>
<img src="images/el_principito_header.jpeg">
</section>

<p>
El principito es una narración corta del escritor francés Antoine de Saint-Exupéry,
</p>
<p>que trata de la historia de un pequeño príncipe que parte de su asteroide a una travesía por el universo,
</p>
<p>
en la cual descubre la extraña forma en que los adultos ven la vida y comprende el valor del amor y la amistad.
</p>

<section>
<iframe width="560" height="315" src="https://www.youtube.com/embed/xMdrJknUCQA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</section>

</body>
&emsp;
<footer>
<img src="images/el_principito_footer.jpeg">
</footer>
<section>
<a href= "https://www.lepetitprince.com/">Official Site</a>
</section>
</html>
EOF

chmod 755 /var/www/html/index.html

# Activar el sitio
a2ensite default.conf

# Inicio de servicio
apachectl -DFOREGROUND
service apache2 restart
exec "$@"